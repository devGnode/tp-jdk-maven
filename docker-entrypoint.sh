#!/bin/sh


sleep 1
#
# DEFINE JAVA_HOME VAR
#
if [ -z "${JAVA_HOME}" ]
then
  echo "Define JAVA_HOME..."
  export JAVA_HOME=$( dirname `update-alternatives --list java` )
  export PATH=$PATH:$JAVA_HOME
  echo "JAVA_HOME=$JAVA_HOME"
fi

#
# Prompt root
#
/bin/sh
