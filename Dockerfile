FROM debian:latest
MAINTAINER marco <madevincre@zenity.fr>

#
# VARIABLE ENV
#
ENV JAVA_HOME="" \
	MAVEN_HOME=/usr/share/maven \
	M2_HOME=/usr/share/maven/conf
#
# MISE A JOUR
#
RUN apt-get update -y
	
#
# WORKSPACE
#
RUN mkdir -p /framework
		
#
# JAVA & MAVEN
#
RUN apt-get install -y openjdk-11-jdk \
	maven

#
# VOLUME
#
VOLUME ["/framework","/usr/share/maven/conf"]


#
# Entrypoint
#
COPY docker-entrypoint.sh /sbin/docker-entrypoint.sh
RUN chmod u+x /sbin/docker-entrypoint.sh

CMD ["/sbin/docker-entrypoint.sh"]
